module.exports = {
  root: true,
  extends: ['airbnb'],
  env: {
    jest: true
  },
  plugins: ['react', 'react-native', 'jsx-a11y', 'jest'],
  rules: {
    'comma-dangle': 'off',
    'object-curly-newline': 'off',
    'react/jsx-filename-extension': 'off',
    'react/no-array-index-key': 'off',
    'import/no-unresolved': [
      'error',
      {
        ignore: ['.json']
      }
    ],
    'react/destructuring-assignment': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/prop-types': ['error', { ignore: ['navigation'] }],
    'padded-blocks': [
      'warn',
      {
        classes: 'always'
      }
    ],
    'react/no-unescaped-entities': [
      'warn',
      {
        forbid: ['>', '}']
      }
    ],
    'no-nested-ternary': 'off',
    'react/static-property-placement': ['off'],
    'no-plusplus': [
      'error',
      {
        allowForLoopAfterthoughts: true
      }
    ],
    'object-curly-newline': [
      'warn',
      {
        ObjectExpression: { consistent: true },
        ImportDeclaration: { multiline: true }
      }
    ],
    'react/jsx-curly-newline': 'off',
    'react/state-in-constructor': 'off',
    'react/jsx-props-no-spreading': 'off'
  }
};
