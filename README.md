# Minesweeper (by Clément Di Domenico)

This repository contains the Minesweeper project. This README explains how the app works and how to setup the project to build and test it.

## Architecture of the app

- The main files of the app are located in `src` in the root of the directory. The `App.js` file in root implement a StackNavigator from `react-navigation` library, which enable the user to navigate between 4 main views. The StackNavigator definition can be found in `src/navigation/gameStackNavigator.js`.

- The views are located in the `src/view` directory. 
    - There is the `landingPage..js`, which one appears first and asks the user if he want to play or read a quick tutorial. 
    - Then the user can acces either the Tutorial Page (in `tutorial.js` ), explaining the rules of the game, or the Level Selection Page (defined in `levelSelection.js`), in order to choose the level of difficulty of the game. As described in the Tutorial, in easy mode the grid is 5 by 5 with 5 mines, in Medium 8 by 8 with 8 mines, and in Hard 10 by 15 with 15 mines to find.
    - After the level selection, the user acces the `gamePage`, with the grid to sweep.

- The view implement various components defined in `src/components`such as a customButton, the mineGrid and the animated endGameModal. Also, **styled-components** are used in Tutorial more specificaly to avoid rewrite the same style property for all the paragraphs.

- `src/styles` stores the styles which can be reused (container, texts) and the colors, in order to have a consistent design.

- To avoid the components to be too dense, some of the hooks are stored in `src/hooks`. It's the case for `useMineGrid.js` which is used to setup the state of the mineGrid component.

- Finally the `src/utils` directory contains the base logic functions to use with the grid, with also a .test file in order to perform basic tests on those functions.

## The MineGrid Logics

- A variable named TileMap is setup in the state of the MineGrid component. It represents the tiles of the grid. It's an array of objects : `{ status: '', value: 0 }`. 
    - Status can have 4 values : **''**, the default case, meaning the tile has not been discovered yet, **'discovered'** if it has been discovered, **'flaged'** if the user has put a flag on it, and **'failed'**, if the user discovered a mine.
    - The value represent the number of mine in the direct neighborhood of the tile.

- The mines are generated randomly and stored in an array 'mines'.

- When a tile is pressed, the app explore the neighborhood of the tile using a graph exploration process (`revealEmpty` function). It is implemented using a pile system : while the pile contains coordinates, it takes the first one. If that one tile has mines in it's neighborhood, it's status is updated with 'discovered'. If it has not, it's direct neighbors are added to the pile and then it's status is updated with 'discovered'.

- To detect win, the app tests if the number of discovered tiles is equal to the total number of tiles minus the number of mines.

## Libraries

- `react-navigation` is used to handle the navigation between the views inside the app.
- `animated` to manage the animation of the EndGameModal.
- The tests are setup and performed with `jest`. To run them, run `yarn test`.
- `styled`: to use very efficient components with custom styles.
- `react-native-exit-app`: to handle the closure of the app when the user press the 'Quit' button in the landing page.
- `eslint` as a linter. The custom rules are in the root directory in `.eslintrc.js`