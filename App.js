import React from 'react';
import 'react-native-gesture-handler';

import GameStackNavigator from './src/navigation/gameStackNavigator';

const App = () => <GameStackNavigator />;

export default App;
