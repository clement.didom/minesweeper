import React from 'react';
import { View, Text } from 'react-native';

import colors from '../styles/colors';
import styles from '../styles/styles';
import CustomButton from '../components/customButton';

const LevelSelectionPage = ({ navigation }) => {
  const handleSelect = (x, y) => navigation.navigate('GamePage', { sizeX: x, sizeY: y });
  return (
    <View style={styles.main}>
      <Text style={styles.defaultText}>Choose your difficulty :</Text>
      <CustomButton title="Easy" color={colors.sweetBlue} onPress={() => handleSelect(5, 5)} />
      <CustomButton title="Medium" onPress={() => handleSelect(8, 8)} />
      <CustomButton title="Hard" color={colors.hardRed} onPress={() => handleSelect(10, 15)} />
    </View>
  );
};

export default LevelSelectionPage;
