import React from 'react';
import { View, Text } from 'react-native';
import styled from 'styled-components/native';

import Styles from '../styles/styles';
import colors from '../styles/colors';
import CustomButton from '../components/customButton';

const TutoText = styled.Text`
  color: ${colors.textWhite};
  font-size: 20;
  text-align: center;
  margin-bottom: 30;
`;

const B = styled.Text`
  font-weight: bold;
`;

const Tutorial = ({ navigation }) => {
  const handleStart = () => navigation.navigate('LevelSelectionPage');
  return (
    <View style={Styles.main}>
      <View style={Styles.textContainer}>
        <Text style={Styles.title}>Tutorial</Text>
        <TutoText>
          First select your <B>difficulty</B>.
        </TutoText>
        <TutoText>
          In the <B>Easy mode</B> you will have to find 5 mines, in{' '}
          <B>Medium</B> 8 and in{' '}
          <B>Hard</B> 15.
        </TutoText>
        <TutoText>
          To discover a tile, just <B>press it</B>. But take care, it might hide
          an <B>explosive surprise</B> !
        </TutoText>
        <TutoText>
          If you think you have found a mine, <B>press long</B> the tile to flag
          it.
        </TutoText>
        <TutoText>
          In order <B>to win</B>, <B>discover</B> all the empty tiles.
        </TutoText>
        <TutoText>Good luck !</TutoText>
        <CustomButton title="Let's go !" onPress={handleStart} />
      </View>
    </View>
  );
};

export default Tutorial;
