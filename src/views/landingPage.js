import React from 'react';
import { View, Text } from 'react-native';

import RNExitApp from 'react-native-exit-app';

import styles from '../styles/styles';
import CustomButton from '../components/customButton';

const LandingPage = ({ navigation }) => {
  const handlePlay = () => navigation.navigate('LevelSelectionPage');
  const handleTutorial = () => navigation.navigate('Tutorial');
  const handleQuit = () => RNExitApp.exitApp();
  return (
    <View style={styles.main}>
      <Text style={styles.title}>Minesweeper</Text>
      <CustomButton title="Play" onPress={handlePlay} />
      <CustomButton title="Tutorial" onPress={handleTutorial} />
      <CustomButton title="Quit" onPress={handleQuit} />
    </View>
  );
};

export default LandingPage;
