import React, { useState } from 'react';
import { View, Text } from 'react-native';

import styles from '../styles/styles';

// Local Imports
import Minegrid from '../components/minegrid';

const GamePage = ({ navigation }) => {
  const sizeX = navigation.getParam('sizeX');
  const sizeY = navigation.getParam('sizeY');
  const [flagCount, setFlagCount] = useState(sizeY);
  const onClose = () => navigation.navigate('LandingPage');
  const updateFlag = (add) => (add ? setFlagCount(flagCount + 1) : setFlagCount(flagCount - 1));
  return (
    <View style={styles.main}>
      <Text style={styles.defaultText}>Flags : {flagCount}</Text>
      <Minegrid
        sizeX={sizeX}
        sizeY={sizeY}
        onClose={onClose}
        updateFlag={updateFlag}
        onReset={() => setFlagCount(sizeY)}
      />
    </View>
  );
};

export default GamePage;
