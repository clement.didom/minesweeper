import {
  arrayContainsCoordinate,
  generateMines,
  generateTileMap,
  mapFormater
} from './gridFunctions';

test('generateRandomInt ', () => {
  expect(generateMines(10, 10).length).toBe(10);
  expect(generateMines(10, 15).length).toBe(15);
});

test('arrayContainsCoordinate ', () => {
  const array = [
    [1, 2],
    [2, 8],
    [9, 0],
    [12, 16]
  ];
  const coordinate1 = [9, 0];
  const coordinate2 = [10, 14];
  expect(arrayContainsCoordinate(array, coordinate1)).toBeTruthy();
  expect(arrayContainsCoordinate(array, coordinate2)).toBeFalsy();
});

test('generateTileMap', () => {
  const tileMap = generateTileMap(10, 15);
  expect(tileMap.length).toBe(10 * 15);
  tileMap.forEach((element) => expect(element).toEqual({ status: '', value: 0 }));
});

test('mapFormater', () => {
  const tileMap = generateTileMap(10, 15);
  expect(mapFormater(tileMap, 10).length).toBe(15);
  mapFormater(tileMap, 10).forEach((element) => expect(element.length).toBe(10));
});
