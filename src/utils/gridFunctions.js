// Basic logic functions used with the grid

/* Takes two int, return a random number bewteen a and b included */
const generateRandomInt = (a, b) => Math.floor(Math.random() * (b - a + 1)) + a;


export const arrayContainsCoordinate = (array, coordinate) => {
  /* Takes an array of coordinates and a coordinate, and return true or false
  wether the coordinate is included in the array or not. */
  for (let i = 0; i < array.length; i++) {
    if (array[i][0] === coordinate[0] && array[i][1] === coordinate[1]) {
      return true;
    }
  }
  return false;
};

export const generateMines = (x, y) => {
  /* Takes 2 int x and y, and return an array of length y with coordinates
  bewteen [0, 0] and [x-1, y-1]. Also check that all the coordinates returned
  are different.
  The result is printed inside the console for tests and debug. */
  const result = [];
  for (let i = 0; i < y; i++) {
    let newMine = [generateRandomInt(0, x - 1), generateRandomInt(0, y - 1)];
    while (arrayContainsCoordinate(result, newMine)) {
      newMine = [generateRandomInt(0, x - 1), generateRandomInt(0, y - 1)];
    }
    result.push(newMine);
  }
  console.log(result);
  return result;
};

export const generateTileMap = (x, y) => {
  /* Takes 2 int x and y, and return an array of length x * y of objects
  {status : '', value: 0}. */
  const res = [];
  for (let i = 0; i < x; i++) {
    for (let j = 0; j < y; j++) {
      res.push({ status: '', value: 0 });
    }
  }
  return res;
};

export const mapFormater = (map, size) => {
  /* Takes an array and a int. Return an array containing arrays of length 'size'
  slieced from 'map'. A copy of 'map' is performed first to avoid modifying it directly */
  const mapCopy = [...map];
  const res = [];
  while (mapCopy.length > 0) {
    res.push(mapCopy.splice(0, size));
  }
  return res;
};
