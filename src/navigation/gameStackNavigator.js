import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LandingPage from '../views/landingPage';
import GamePage from '../views/gamePage';
import Tutorial from '../views/tutorial';
import LevelSelectionPage from '../views/levelSelection';

const GameStackNavigator = createStackNavigator(
  {
    LandingPage,
    LevelSelectionPage,
    GamePage,
    Tutorial
  },
  {
    initialRouteName: 'LandingPage'
  }
);

export default createAppContainer(GameStackNavigator);
