import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import styles from '../styles/styles';

import Tile from './tile';
import EndGameModal from './endGameModal';

import {
  arrayContainsCoordinate,
  generateMines,
  generateTileMap,
  mapFormater
} from '../utils/gridFunctions';
import useMineGrid from '../hooks/useMineGrid';

const Styles = StyleSheet.create({
  row: { flexDirection: 'row' }
});

const Minegrid = ({ sizeX, sizeY, onClose, updateFlag, onReset }) => {
  const {
    displayModal,
    setDisplayModal,
    mines,
    setMines,
    tileMap,
    setTileMap,
    win,
    setWin
  } = useMineGrid(sizeX, sizeY);

  const reset = () => {
    /* Hide the End Game Modal.
    Resets the flag number and the TileMap to their initial value.
    Generates a new set of Mines */
    onReset();
    setDisplayModal(false);
    setTileMap(generateTileMap(sizeX, sizeY));
    setMines(generateMines(sizeX, sizeY));
  };

  const extractTiles = () => {
    /* Returns the number of discovered tiles from the TileMap */
    let count = 0;
    tileMap.forEach((element) => {
      if (element.status === 'discovered') {
        count += 1;
      }
    });
    return count;
  };

  const testVictory = () => {
    /* Set the state variable win to true if the number of dicovered tiles
    is equal to the total number of tiles minus the number of mines. */
    if (extractTiles() === sizeY * (sizeX - 1)) {
      setWin(true);
      setDisplayModal(true);
    }
  };

  const computeMines = (coordinate) => {
    /* Takes a coordinate. Returns the number of mines in the neighborhood of coordinate. */
    let count = 0;
    for (let i = -1; i <= 1; i++) {
      if (coordinate[0] + i >= 0 && coordinate[0] + i < sizeX) {
        for (let j = -1; j <= 1; j++) {
          if (coordinate[1] + j >= 0 && coordinate[1] + j < sizeY) {
            const coordinateToTest = [coordinate[0] + i, coordinate[1] + j];
            if (arrayContainsCoordinate(mines, coordinateToTest)) {
              count += 1;
            }
          }
        }
      }
    }
    return count;
  };

  const updateTile = (coordinate, status, value) => {
    /* Upadte the status and the value of a given coordinate in the TileMap. */
    const newTileMap = [...tileMap];
    const index = coordinate[1] * sizeX + coordinate[0];
    newTileMap[index].status = status;
    newTileMap[index].value = value;
    setTileMap(newTileMap);
    testVictory();
  };

  const flagHandler = (coordinate, status, value) => {
    /* Update the number of flags in the gamePage view, and the coresponding tile
    in the TileMap. */
    if (status === 'flaged') {
      updateFlag(false);
    } else {
      updateFlag(true);
    }
    updateTile(coordinate, status, value);
  };

  /* Returns the status of a tile in TileMap from its coordinate. */
  const getTileStatus = (coordinate) => tileMap[coordinate[1] * sizeX + coordinate[0]].status;

  const checkTile = (coordinate) => {
    /* Check if the tile is a Mine.
    If the tile is a mine, the End Game Modal appears with the lose message.
    If not, the tile is updated with the status 'discovered' */
    if (!displayModal) {
      if (arrayContainsCoordinate(mines, coordinate)) {
        setDisplayModal(true);
        updateTile(coordinate, 'failed', computeMines(coordinate));
      } else {
        updateTile(coordinate, 'discovered', computeMines(coordinate));
      }
    }
  };

  const revealEmpty = (coordinate) => {
    /* Reveal the empty tiles around a given one by performing a graph exploration
    of the neighborhood. */
    const toVisit = [coordinate];
    while (toVisit.length > 0) {
      const inVisit = toVisit[0];
      if (computeMines(inVisit) === 0 && getTileStatus(inVisit) === '') {
        for (let i = -1; i <= 1; i++) {
          if (inVisit[0] + i >= 0 && inVisit[0] + i < sizeX) {
            for (let j = -1; j <= 1; j++) {
              if (inVisit[1] + j >= 0 && inVisit[1] + j < sizeY) {
                const coordinateToTest = [inVisit[0] + i, inVisit[1] + j];
                if (computeMines(coordinateToTest) === 0) {
                  toVisit.push(coordinateToTest);
                } else {
                  checkTile(coordinateToTest);
                }
              }
            }
          }
        }
      }
      checkTile(inVisit);
      toVisit.shift();
    }
  };

  return (
    <View style={styles.main}>
      <View>
        {mapFormater(tileMap, sizeX).map((row, i) => (
          <View key={i} style={Styles.row}>
            {row.map((tile, j) => (
              <Tile
                key={[j, i]}
                coordinate={[j, i]}
                status={tile.status}
                value={tile.value}
                press={revealEmpty}
                onFlag={flagHandler}
              />
            ))}
          </View>
        ))}
      </View>
      {displayModal && (
        <EndGameModal
          onCloseNo={onClose}
          onCloseYes={reset}
          modalText={win ? 'You win !' : 'You lose ...'}
        />
      )}
    </View>
  );
};

Minegrid.propTypes = {
  sizeX: PropTypes.number.isRequired,
  sizeY: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
  updateFlag: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired
};

export default Minegrid;
