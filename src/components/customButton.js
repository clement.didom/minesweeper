import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  buttonContainer: {
    margin: 10,
    width: 150,
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: 'white',
    fontSize: 25
  }
});

const CustomButton = ({ title, onPress, color, textColor }) => (
  <TouchableOpacity
    style={[styles.buttonContainer, { backgroundColor: color || '#d6870d' }]}
    onPress={onPress}
  >
    <Text style={[styles.buttonText, { color: textColor }]}>{title}</Text>
  </TouchableOpacity>
);

CustomButton.defaultProps = {
  color: '#d6870d',
  textColor: 'white'
};

CustomButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  textColor: PropTypes.string
};

export default CustomButton;
