import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  modal: {
    padding: 10,
    position: 'absolute',
    width: 200,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});

const EndGameModal = ({ modalText, onCloseYes, onCloseNo }) => {
  const [moveUp] = useState(new Animated.Value(500));
  const [scale] = useState(new Animated.Value(1));
  useEffect(() => {
    Animated.sequence([
      Animated.timing(moveUp, {
        toValue: 0,
        easing: Easing.out(Easing.ease),
        duration: 1000
      }),
      Animated.timing(scale, {
        toValue: 1.4,
        duration: 500
      }),
      Animated.timing(scale, {
        toValue: 1,
        duration: 500
      })
    ]).start();
  }, []);
  return (
    <Animated.View
      style={[styles.modal, { transform: [{ translateY: moveUp }, { scale }] }]}
    >
      <Text>{modalText}</Text>
      <Text>Do you want to play again ?</Text>
      <View style={styles.buttonContainer}>
        <Button title="Yes" onPress={onCloseYes} />
        <Button title="No" onPress={onCloseNo} />
      </View>
    </Animated.View>
  );
};

EndGameModal.propTypes = {
  modalText: PropTypes.string.isRequired,
  onCloseYes: PropTypes.func.isRequired,
  onCloseNo: PropTypes.func.isRequired
};

export default EndGameModal;
