import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  standardTile: {
    width: 32,
    height: 32,
    backgroundColor: '#008A32',
    borderWidth: 2,
    borderColor: 'rgba(255, 255, 255, 0.4)',
    borderRadius: 3,
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 1,
    margin: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mine: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 10
  },
  flag: {
    width: 24,
    height: 24,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 12,
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const Mine = () => <View style={styles.mine} />;

const Flag = () => (
  <View style={styles.flag}>
    <Text>?</Text>
  </View>
);

const Tile = ({ coordinate, value, status, press, onFlag }) => {
  const handlePress = () => {
    if (status === '') {
      press(coordinate);
    }
  };

  switch (status) {
    case 'discovered':
      return (
        <View style={[styles.standardTile, { backgroundColor: 'grey' }]}>
          {value > 0 && <Text>{value}</Text>}
        </View>
      );
    case 'flaged':
      return (
        <TouchableOpacity
          style={styles.standardTile}
          onLongPress={() => onFlag(coordinate, '', 0)}
        >
          <Flag />
        </TouchableOpacity>
      );
    case 'failed':
      return (
        <View style={[styles.standardTile, { backgroundColor: 'red' }]}>
          <Mine />
        </View>
      );
    default:
      return (
        <TouchableOpacity
          style={styles.standardTile}
          onPress={handlePress}
          onLongPress={() => onFlag(coordinate, 'flaged', 0)}
        />
      );
  }
};

Tile.propTypes = {
  coordinate: PropTypes.arrayOf(PropTypes.number).isRequired,
  value: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  press: PropTypes.func.isRequired,
  onFlag: PropTypes.func.isRequired
};

export default Tile;
