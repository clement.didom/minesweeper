import { useEffect, useState } from 'react';

import { generateTileMap, generateMines } from '../utils/gridFunctions';

const useMineGrid = (sizeX, sizeY) => {
  const [displayModal, setDisplayModal] = useState(false);
  const [mines, setMines] = useState([]);
  const [tileMap, setTileMap] = useState([]);
  const [win, setWin] = useState(false);
  useEffect(() => {
    setTileMap(generateTileMap(sizeX, sizeY));
    setMines(generateMines(sizeX, sizeY));
    setDisplayModal(false);
  }, []);

  return { displayModal, setDisplayModal, mines, setMines, tileMap, setTileMap, win, setWin };
};

export default useMineGrid;
