import { StyleSheet } from 'react-native';

import colors from './colors';

const Styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.strongBlue
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 20,
    marginVertical: 10,
    alignItems: 'center'
  },
  title: {
    color: colors.markerBlue,
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 30
  },
  defaultText: {
    color: colors.textWhite,
    fontSize: 20,
    fontWeight: 'bold',
    margin: 15
  }
});

export default Styles;
