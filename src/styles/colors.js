const colors = {
  strongBlue: '#00aeb0',
  deepOrange: '#d6870d',
  hardRed: '#db3300',
  sweetBlue: '#2e40e6',
  markerBlue: '#274edb',
  textWhite: '#ccdeda'
};

export default colors;
